# liteos-rs

#### 介绍
尝试用rust重写liteos内核，使用RUST语言级的安全(类型和内存安全)减少对当前硬件隔离的依赖

计划分如下节奏：

1.  基于STM32F407搭建可以跑LiteOS的qemu工程小系统工程；
2.  使用RUST实现内核部分；

#### 软件架构
软件架构说明


#### 安装教程
rust stable安装
1.  rustup 安装
2.  cargo 安装
3.  rustup target add thumbv6m-none-eabi thumbv7m-none-eabi thumbv7em-none-eabi thumbv7em-none-eabihf
4.  [xpack-qemu-arm-2.8.0-12-linux-x64.tar](https://github.com/xpack-dev-tools/qemu-arm-xpack/releases/)  下载,解压 bin配置到PATH

rust nightly安装(使用嵌入式汇编时，需切换到nightly版本)
1.  rustup install nightly
2.  rustup default nightly
3.  rustup target add thumbv7em-none-eabi

#### 使用说明

后续基于STM32F407来调试，因qemu-system-arm不支持STM32F407单板，所以qemu采用qemu-system-gnuarmeclipse

编译执行：
1.  cargo build
2.  cargo run

#### 运行效果

输出: hello, liteos

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
